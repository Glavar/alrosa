export const arrayHashes = [
    {
        test: 'name',
        value: 'A0EHzdTq5uG7',
    }, {
        test: +(new Date('2000-01-01 15:00:00')),
        value: 'xmASLas78hXmT7KC5KtKXhBMUmjUGpU4eUmQ',
}];

export const ticketMock = 'w9CP-Ko2R-qnxa-RV41';
export const hashMock = 'A0EHzdTq5uG7';
export const keyMock = 'kA4uv1SODUXLHKKFgNh9kfdJtx5uaaFoYSJxUg1hmLcaaF3RsZysLoHDQ';
export const codeMock = 'pIelIK21H-L7JM6DmzQ-McvcLZmhg';
export const tockenMock = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9';
