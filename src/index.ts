import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as json from 'koa-json';
import * as Router from 'koa-router';
import initRouter from './router';
import Logger, {getLog} from './utils/logger';

const app = new Koa();
const koaRouter = new Router();
const logPino = getLog('Server');

app.use(json({ pretty: false }));
app.use(bodyParser());
app.use(Logger({logPino, enable: false}));

initRouter(koaRouter);

app
    .use(koaRouter.routes())
    .use(koaRouter.allowedMethods());

app.listen(7009, () => {
    logPino.info('server started');
});
