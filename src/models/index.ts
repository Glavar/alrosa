import * as mongoose from 'mongoose';
import {dataBase, dataBaseConfig} from '../config';
import {getLog} from '../utils/logger';
const logPino = getLog('DataBase');

mongoose
    .connect(dataBase, dataBaseConfig)
    .then(() => {
        logPino.info('MongoDB event connected');
    })
    .catch(logPino.error);

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const Mixed = Schema.Types.Mixed;

export {
    Schema,
    ObjectId,
    Mixed,
};

mongoose.connection.once('open', () => {
    logPino.info('MongoDB event open');

    mongoose.connection.on('connected', () => {
        logPino.info('MongoDB event connected');
    });

    mongoose.connection.on('disconnected', () => {
        logPino.warn('MongoDB event disconnected');
    });

    mongoose.connection.on('reconnected', () => {
        logPino.info('MongoDB event reconnected');
    });

    mongoose.connection.on('error', (err) => {
        logPino.error('MongoDB event error: ', err);
    });
});
