import * as mongoose from 'mongoose';
import {IBetTokenModel} from '../interfaces';
import {Schema} from './index';

const BetTokensShema = new Schema(
    {
        token: {
            type: String,
            required: true,
        },
        userId: {
            type: String,
            required: true,
            index: true,
        },
        bargainId: {
            type: String,
            required: true,
            index: true,
        },
        beginDate: {
            type: Date,
            default: Date.now,
        },
        expiredDate: {
            type: Date,
        },
    },
    {
        timestamps: true,
    },
);

export const BetToken = mongoose.model<IBetTokenModel>('BetToken', BetTokensShema);

export default BetToken;
