import * as Koa from 'koa';
import {IBetInfo, IBetToken} from '../interfaces';
import BetToken from '../models/BetTokens';
import redis from '../redis';
import decodeToken from '../utils/decodeToken';
import genCSRF from '../utils/genCSRF';
import genHashEncode from '../utils/genHashEncode';
import getCode from '../utils/getCode';
import getTokenData from '../utils/getTokenData';
import ticketGenerator from '../utils/ticketGenerator';

export default class CryptClass {

    public getTicket = (ctx: Koa.Context) => {
        return this.render(ctx, ticketGenerator());
    }

    public saveData = async (ctx: Koa.Context) => {
        const {control, key, ...data} = ctx.request.body;
        const code = getCode(key, control);
        const dataToken = getTokenData(data, code);

        const token = new BetToken({
            token: dataToken,
            ...data,
        });
        await token.save();

        this.render(ctx);
    }

    public getUserToken = async (ctx: Koa.Context) => {
        this.render(ctx, await this.genCSRF(ctx));
    }

    public getUserPrice = async (ctx: Koa.Context) => {
       const {price} = await this.getUserInfoToken(ctx);
       const {csrf} = await this.genCSRF(ctx);
       this.render(ctx, {price: +price, csrf});
    }

    public getUserBargain = async (ctx: Koa.Context) => {
       const {bargainId} = await this.getUserInfoToken(ctx);
       const {csrf} = await this.genCSRF(ctx);
       this.render(ctx, {bargainId, csrf});
    }

    private getUserInfoToken = async (ctx: Koa.Context): Promise<IBetInfo> => {
        const {control, key, userId, bargainId, csrf} = ctx.request.body;

        if (csrf !== await this.getCSRF(userId)) {
            (console as any).error('csrf bad');
        }

        const code = getCode(key, control);
        const {token} = await BetToken
            .findOne({userId, bargainId})
            .select('token')
            .lean().exec() as IBetToken;

        return decodeToken(token, code);
    }

    private async genCSRF(ctx: Koa.Context) {
        const {userId} = ctx.request.body;
        const csrfCode = genCSRF();
        const csrf = genHashEncode(userId, csrfCode);
        await this.setCSRF(userId, csrf);
        return {csrf};
    }

    private getCSRF(userId: string) {
        return redis.get(userId);
    }

    private setCSRF(userId: string, csrf: string) {
        return redis.set(userId, csrf);
    }

    private render(ctx: Koa.Context, data = {}) {
        if (data === false) {
            return ctx.body = { result: data };
        }
        return ctx.body = {
            result: true,
            data,
        };
    }

}
