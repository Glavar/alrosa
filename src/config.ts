import {IConfig} from './interfaces';

const config: IConfig = {
    logLevel: 'debug',
};

export const dataBase = 'mongodb://localhost/alrosa';
export const dataBaseConfig = {
    autoIndex: false,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    poolSize: 10,
    bufferMaxEntries: 0,
};

export const logConfig = {level: config.logLevel, safe: true};
