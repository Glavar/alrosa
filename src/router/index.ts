import * as Router from 'koa-router';
import CryptClass from '../controllers/Crypt';

const CryptElement = new CryptClass();
const API_VERSION = '/api/v1';

export default (router: Router) => {

    router.get(`${API_VERSION}/ticket`, CryptElement.getTicket);

    router.post(`${API_VERSION}/user/token`, CryptElement.getUserToken);

    router.post(`${API_VERSION}/user/price`, CryptElement.getUserPrice);

    router.post(`${API_VERSION}/user/bargain`, CryptElement.getUserBargain);

    router.post(`${API_VERSION}/new/bet`, CryptElement.saveData);

};
