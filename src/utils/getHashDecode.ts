const Hashids = require('hashids');

export default (code: string, salt = 'alros'): string => {
    const hashids = new Hashids(salt);
    const decoded = hashids.decode(code);
    return String.fromCharCode(...decoded.map((value: number, index: number) => value + index - decoded.length));
};
