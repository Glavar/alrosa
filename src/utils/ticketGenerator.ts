import genHashEncode from './genHashEncode';
import genTicket from './genTicket';

export default () => {
    const ticket = genTicket();
    const control = genHashEncode();
    const key = genHashEncode(ticket, control);

    return {
        control,
        key,
    };
};
