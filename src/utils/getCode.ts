import getHashDecode from './getHashDecode';

export default (key: string, control: string): string => getHashDecode(key, control);
