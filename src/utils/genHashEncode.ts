const Hashids = require('hashids');

export default (name: string | number = +new Date(), salt = 'alros') => {
    const hashids = new Hashids(salt);
    name = `${name}`;
    return hashids.encode(
        name
            .split('')
            .map((char, index) => char.charCodeAt(0) - index + (name  as string).length),
    );
};
