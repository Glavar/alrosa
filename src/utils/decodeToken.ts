import * as jwt from 'jsonwebtoken';
import {IBetInfo} from '../interfaces';
export default (token: string, secret: string): IBetInfo => jwt.verify(token, secret) as IBetInfo;
