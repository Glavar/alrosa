import * as jwt from 'jsonwebtoken';

export default (data: any, code: string) => jwt.sign(data, code);
