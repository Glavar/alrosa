import makeId from '../utils/makeid';

export default (countElements = 4, lengthId = 4) => {
    const arrayCodes = [];
    while (countElements--) {
        arrayCodes.push(makeId(lengthId));
    }
    return arrayCodes.join('-');
};
