import makeId from './makeid';

export default () => makeId(16);
