import {Document} from 'mongoose';

export interface IConfig {
    logLevel: string;
}

export interface ICode {
    code: string;
}

export interface IBetToken {
    token: string;
    userId: string;
    bargainId: string;
    controlId: string;
    beginDate: Date;
    expiredDate: Date;
}

export interface IUserToken {
    token: string;
    userId: string;
    bargainId: string;
    controlId: string;
    beginDate: Date;
    expiredDate: Date;
}

export interface IBetInfo {
    userId: string;
    bargainId: string;
    price: number;
}

export interface IBetTokenModel extends IBetToken, Document { }

export interface IUserTokenModel extends IUserToken, Document { }
