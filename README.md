# API

### First step
*When creating a new lot need send request on get key for it*
**ONLY ONE KEY ON ONE LOT**

|Link|Type|Body|
|:---:|:---:|:---:|
|`/api/v1/ticket`| GET | - |

**Answer:**
```
{
"result": true,
"data": {
"control": "5kVUPjCbgfO6CA5f6qHNKUpEhz7c5mIxwhzdS8J",
"key": "KEEHVXSn5srmiYqUj6t0xsKdfxAimvhoXcGLtePC6pSWQSD4Iv5FA6HYy"
}
}
```
[Example](http://prntscr.com/i7kt9i)

### Second step
*When creating a new bet in lot*

|Link|Type|Body|
|:---:|:---:|:---:|
|`/api/v1/new/bet`| POST | userId, bargainId, price, control, key  |

**Answer:**
```
{
   "result": true,
   "data": {}
}
```
[Example](http://prntscr.com/i7kvlh)

### Third step
*Temp token for client*

|Link|Type|Body|
|:---:|:---:|:---:|
|`/api/v1/user/token`| POST | userId  |

**Answer:**
```
{
    "result": true,
    "data": {
        "csrf": "zPMuODulNujmuzk"
    }
}
```
[Example](http://prntscr.com/i7kvwt)

### Get user price
|Link|Type|Body|
|:---:|:---:|:---:|
|`/api/v1/user/price`| POST | userId, bargainId, control, key, csrf  |
```
{
"result": true,
"data": {
"price": 20000,
"csrf": "REJCJXCxpCDKC6A"
}
}
```
[Example](http://prntscr.com/i7kx1h)



