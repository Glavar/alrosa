const nodeExternals = require('webpack-node-externals');
const {resolve} = require('path');
const webpack = require('webpack');
const BellOnBundlerErrorPlugin = require('bell-on-bundler-error-plugin');
const WebpackErrorNotificationPlugin = require('webpack-error-notification');

module.exports = {
    entry: {
        index: './src/index.ts'
    },
    cache: false,
    target: "node",
    devtool: 'inline-source-map',
    resolve: {
        modules: ['node_modules'],
        extensions: [".ts", ".tsx", ".js"],
        descriptionFiles: ['package.json'],
        moduleExtensions: ['-loader']
    },
    output: {
        path: resolve(__dirname, 'dist'),
        filename: '[name].js',
        publicPath: '/',
        libraryTarget: 'commonjs2'
    },
    externals: [nodeExternals()],
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.ts(x?)$/,
                use: "source-map-loader",
                exclude: '/node_modules/'
            },
            {
                test: /\.ts(x?)$/,
                use: [
                    {loader: 'awesome-typescript-loader'}
                ],
                exclude: /node_modules/,
            }
        ]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            debug: true,
            options: {
                public: true,
                progress: true,
                configuration: {
                    devtool: 'sourcemap'
                }
            },
            root: resolve(__dirname, '..')
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        }),
        new webpack.NamedModulesPlugin(),
        new WebpackErrorNotificationPlugin(),
        new BellOnBundlerErrorPlugin(),
    ],
    node: {
        console: false,
        global: true,
        process: true,
        Buffer: true,
        __filename: true,
        __dirname: true,
        setImmediate: true
    },
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
    }
};